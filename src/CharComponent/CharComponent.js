import React from 'react';

const CharComponent = (props) => {
    return (
            <div className="char" onClick={props.click}>{props.char}</div>
    );
}

export default CharComponent;